# Airjoy Controller

PIC12F675 firmware for a joystick.  Runs 2 ADC channels for X and Y, one key for Z, and outputs 5 bytes per frame.