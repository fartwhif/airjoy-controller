/*
 * File:   newmain.c
 * Author: matth
 *
 * Created on February 25, 2021, 12:48 AM
 */
#include "config.h"
#include "util.h"
#include <xc.h>
#include "newmain.h"
#include "ser.h"

int i = 0;

unsigned char joyState[3] = {
    0, //AN0 least significant 8 bits
    0, //AN1 least significant 8 bits
    0, //unused bits 7-5; key bit 4; most significant 2 bits AN1 3,2 ;most significant 2 bits AN0 1,0;
};

unsigned char frameStartOneOfTwo = 0b10101010;
unsigned char frameStartTwoOfTwo = 0b01010101;

void main() {
    init_ports();
    while (1) {
        analog_measure();
        get_key();
        print(frameStartOneOfTwo); //frame start 1/2
        print(frameStartTwoOfTwo); //frame start 2/2
        print(joyState[0]);
        print(joyState[1]);
        print(joyState[2]);
    }
}

void output_high(int nop) {
    if (nop == 0)
        NOP();
    GPIO = 0xFF; //(1 << 4);
    if (nop == 2)
        NOP();
}

void output_low(int nop) {
    if (nop == 0)
        NOP();
    GPIO = 0;
    if (nop == 2)
        NOP();
}

void get_key(void) {
    joyState[2] = ((((GPIO & (1 << Z_KEY))) ? 0 : 1) << 4) | joyState[2];
}

unsigned char adcChannels[2] = {
    0b10000001, // right justified, VDD voltage ref, AN0, not in progress, enabled
    0b10000101 // right justified, VDD voltage ref, AN1, not in progress, enabled
};

void analog_measure() {
    for (i = 0; i < 2; i++) {
        delay_us(8); //acquisition delay good/bad: 1b 5b 7g 6b ( 8b 9b 10b slightly bad)
        //delay_ms(20);
        ADCON0 = adcChannels[i];
        ADCON0 |= 1 << 1; // set go bit to 1
        do {
            //NOP();
        } while ((ADCON0 >> 1) & 1); // wait until go bit is 0
        joyState[i] = ADRESL;
        if (i == 0) {
            joyState[2] = ADRESH;
        } else {
            joyState[2] = ADRESH << 2 | joyState[2];
        }
    }
}

void init_ports() {

    //GPIO bits:
    //  bit 7-6 Unimplemented: Read as ?0?
    //  bit 5-0 GPIO<5:0>: General Purpose I/O pin
    //    1 = Port pin is >VIH
    //    0 = Port pin is <VIL

    //GPIO = 0;

    //TRISIO bits:
    //  bit 7-6 Unimplemented: Read as ?0?
    //  bit 5-0 TRISIO<5:0>: General Purpose I/O Tri-State Control bit
    //    1 = GPIO pin configured as an input (tri-stated)
    //    0 = GPIO pin configured as an output
    //  Note: TRISIO<3> always reads ?1?.

    TRISIO = 0b00001011; // GPIO 0,1,3 are inputs, others are output

    //ANSEL bits:
    //bit 7 Unimplemented: Read as ?0?
    //bit 6-4 ADCS<2:0>: A/D Conversion Clock Select bits
    //  000 = Fosc/2
    //  001 = Fosc/8
    //  010 = Fosc/32
    //  x11 = FRC (clock derived from a dedicated internal oscillator = 500 kHz max)
    //  100 = Fosc/4
    //  101 = Fosc/16
    //  110 = Fosc/64
    //bit 3-0 ANS3:ANS0: Analog Select bits
    //  (Between analog or digital function on pins AN<3:0>, respectively.)
    //  1 = Analog input; pin is assigned as analog input(1)
    //  0 = Digital I/O; pin is assigned to port or special function
    //  Note 1: 
    //      Setting a pin to an analog input automatically disables the digital input circuitry, weak pull-ups, and interrupt-on-change. 
    //      The corresponding TRISIO bit must be set to Input mode in order to allow external control
    //  of the voltage on the pin.

    //maybe only sel one of the two AN[] ?
    ANSEL = 0b01100011; // Fosc/64 clock, AN0,AN1 analog

    //    CMCON = 7;

    //ADCON0 bits:
    //bit 7 ADFM: A/D Result Formed Select bit
    //  1 = Right justified
    //  0 = Left justified
    //bit 6 VCFG: Voltage Reference bit
    //  1 = VREF pin
    //  0 = VDD
    //bit 5-4 Unimplemented: Read as ?0?
    //bit 3-2 CHS1:CHS0: Analog Channel Select bits
    //  00 = Channel 00 (AN0)
    //  01 = Channel 01 (AN1)
    //  10 = Channel 02 (AN2)
    //  11 = Channel 03 (AN3)
    //bit 1 GO/DONE: A/D Conversion Status bit
    //  1 = A/D conversion cycle in progress. Setting this bit starts an A/D conversion cycle.
    //   This bit is automatically cleared by hardware when the A/D conversion has completed.
    //  0 = A/D conversion completed/not in progress
    //bit 0 ADON: A/D Conversion Status bit
    //  1 = A/D converter module is operating
    //  0 = A/D converter is shut-off and consumes no operating current

    ADCON0 = 0b10000001; // right justified, VDD voltage ref, AN0, not in progress, enabled

}


