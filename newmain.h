/* 
 * File:   newmain.h
 * Author: matth
 *
 * Created on February 28, 2021, 9:30 PM
 */

#ifndef NEWMAIN_H
#define	NEWMAIN_H

void get_key(void);
void init_ports();
void output_high(int);
void output_low(int);
void analog_measure();

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* NEWMAIN_H */

