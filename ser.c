/*
 * File:   ser.c
 * Author: matth
 *
 * Created on February 28, 2021, 9:25 PM
 */

#include "config.h"
#include "util.h"
#include <xc.h>
#include "newmain.h"
#include "ser.h"

//////////////////////////////////////////////////////////////////////

#define duration1 89 //had to dial this in for 9600, with 20mhz crystal 84-94 seems to work, using the nominal value
#define duration2 (duration1 * 10) + (duration1 *1.5) //had to dial this in for 9600
 
void print(unsigned short chr) {
    unsigned short mask = 1, i;
    unsigned int txdata;
    txdata = chr;
    output_low(1);
    delay_us(duration1);
    for (i = 0; i < 8; i++) {
        if (mask & txdata) {
            output_high(2);
        } else {
            output_low(0);
        }
        mask <<= 1;
        delay_us(duration1);
    }
    output_high(1);
    delay_us(duration2);
}